(ns spages.core
  (:require [clojure.edn            :as edn]
            [clojure.java.io        :as io]
            [clojure.walk           :as walk]
            [hiccup.util            :refer [escape-html]]
            [markdown.core          :refer [md-to-html-string]]
            [spages.utils           :as utils]
            [spages.types.text      :as text]
            [spages.types.markdown  :as markdown]
            [spages.types.site      :as site]
            [spages.types.menu      :as menu]
            [spages.io.toedn        :refer [convert-to-edn]]
            [spages.io.tohtml       :refer [render-to-html]]
            [spages.io.tomenu       :refer [make-menu]]
            [spages.io.fromresource :refer [load-from-resource]]))

(defn read-string-with-types
  "Gets string with edn parse him and returns model with Site, Page, Dir,
  MenuDir, MenuItem, Text, TextFile, Markdown and MarkdownFile records"
  [edn-string]
  (edn/read-string 
      {:readers {'Site         site/edn->Site 
                 'Page         site/edn->Page
                 'Dir          site/edn->Dir
                 'MenuDir      menu/edn->MenuDir
                 'MenuItem     menu/edn->MenuItem
                 'Text         text/edn->Text
                 'TextFile     text/edn->TextFile
                 'Markdown     markdown/edn->Markdown
                 'MarkdownFile markdown/edn->MarkdownFile}}
      edn-string))

(defn parse-edn-resource 
  "Gets file with edn resources parse him and returns model with Site, Page,
  MenuDir, MenuItem, Text, TextFile, Markdown and MarkdownFile records.
  Throws exception if file does not exist"
  [file]
  (-> file
      utils/open-resource-if-exist 
      read-string-with-types))

(defn make-routing-table 
  "Gets model with Site, Page, Dir, MenuDir, MenuItem, Text, TextFile, Markdown
  and MarkdownFile records and converts to simple map. Where keys are slugs and
  values are vector with Pages or map with context of Page"
  [model]
  (->> model
       (walk/prewalk (fn [i]
                       (cond
                         (site/Site? i) (into [:child] (:child i))
                         (site/Dir? i)  {(:slug i) (into [:child] (:child i))} 
                         (site/Page? i) {(:slug i) (into {} i)}
                         :else i)))
       (walk/prewalk (fn [i]
                       (if (and
                             (vector? i)
                             (= (first i) :child))
                         (into {} (reverse (rest i)))
                         i)))))

(comment 
  (make-menu-1 (-> "main.edn"
                   (parse-edn-resource)
                   (load-from-resource)
                   (render-to-html)))
  (def t-routing-table-1 (make-routing-table (-> "main4.edn"
                                                 (parse-edn-resource)
                                                 (load-from-resource)
                                                 (render-to-html))))
  ;; 0.03 - 0.04 ms
  (time (get-in t-routing-table-1 [:index]))
  (time (get-in t-routing-table-1 [:main :test-dir]))
  (time (get-in t-routing-table-1 [:main :test-dir :test-page]))
  (time (get-in t-routing-table-1 [:main :about]))
  (time (get-in t-routing-table-1 [:main :index]))
  (def t-routing-table-2 (make-routing-table (-> "main5.edn"
                                                 (parse-edn-resource)
                                                 (load-from-resource)
                                                 (render-to-html))))
  (time (get-in t-routing-table-2 [:index]))
  )

(defn get-dir 
  "Returns spages.types.Dir in a nested spages.types.Site or Dir structure,
  where path is the arguments list of :slug in nested structure. Returns
  nil if the path is not present."
  [site-or-dir & path]
  (let [path    (map keyword path)
        dir-map (:child site-or-dir)]
    (loop [dir-path path
           dirs     dir-map]
      (let [f-path (first dir-path)
            f-dir  (first (filter #(= (:slug %) f-path) dirs))]
        (when (some? f-dir)
          (if (empty? (rest dir-path))
            f-dir
            (recur
              (rest dir-path)
              (:child f-dir))))))))

(defn get-page
  "Returns spages.types.Page in a nested spages.types.Site or Dir structure,
  where path is the arguments list of :slug in nested structure. Returns
  nil if the path is not present."
  [site-or-dir & path]
  (let [path     (map keyword path)
        slug     (last path)
        filter-f (fn [i] (first (filter #(= (:slug %) slug) i)))]
    (if-let [dir-path (butlast path)]
      (if-let [f-dir (apply get-dir site-or-dir dir-path)]
        (filter-f (:child f-dir)))
      (filter-f (:child site-or-dir)))))

(comment 
  (-> "main.edn"
      (parse-edn-resource)
      (load-from-resource)
      (render-to-html)
      )
  (make-routing-table (-> "main.edn"
                          (parse-edn-resource)
                          (load-from-resource)
                          (render-to-html)))
  (-> "main.edn"
      (parse-edn-resource)
      (load-from-resource)
      (render-to-html)
      (make-menu)
      )
  (get-page
    (-> "main3.edn"
        (parse-edn-resource)
        (load-from-resource)
        (render-to-html))
    :main :test-dir :test-page)
  (get-page
    (-> "main3.edn"
        (parse-edn-resource)
        (load-from-resource)
        (render-to-html))
    "main" "test-dir" "test-page")
  (get-dir
    (-> "main3.edn"
        (parse-edn-resource)
        (load-from-resource)
        (render-to-html))
    :main :test-dir) 

  (def t-tree (-> "main4.edn"
                  (parse-edn-resource)
                  (load-from-resource)
                  (render-to-html)))
  ;; 0.06 - 0.10 ms
  (time (get-page t-tree :index))
  (time (get-dir t-tree :main :test-dir))
  (time (get-page t-tree :main :test-dir :test-page))
  (time (get-page t-tree :main :about))
  (time (get-page t-tree :main :index))
  )

