(ns spages.types.text
  (:require [hiccup.util            :refer [escape-html]]
            [spages.io.toedn        :refer [IConvertToEDN]]
            [spages.io.tohtml       :refer [IRenderToHTML]]
            [spages.io.fromresource :refer [ILoadFromResource]]
            [spages.io.tomenu       :refer [IConvertToMenu]]
            [spages.utils           :refer [open-resource-if-exist]]))

;;-| Type for text
;;-------------------------------------------------------------------------------

(defrecord Text [content])

(defn Text? 
  "Check given argument i is Text"
  [i]
  (= spages.types.text.Text (type i)))

(defn Text->edn 
  "Creates a different edn format for the Text"
  [^Text text]
  (str "#Text " (prn-str (:content text))))

(defn edn->Text 
  "Takes the alternative edn and convert it into a Text"
  [c]
  (->Text c))

(extend-type Text
  IConvertToEDN
  (convert-to-edn [this]
    (Text->edn this))
  IRenderToHTML
  (render-to-html [{:keys [content]}]
    (when content (escape-html content)))
  ILoadFromResource
  (load-from-resource [this] this)
  IConvertToMenu
  (convert-to-menu [this _] this))

;;-| Type for file with text
;;-------------------------------------------------------------------------------

(defrecord TextFile [content])

(defn TextFile? 
  "Check given argument i is TextFile"
  [i]
  (= spages.types.text.TextFile (type i)))

(defn TextFile->edn 
  "Creates a different edn format for the TextFile"
  [^TextFile f]
  (str "#TextFile " (prn-str (:content f))))

(defn edn->TextFile 
  "Takes the alternative edn and convert it into a TextFile"
  [c]
  (->TextFile c))

(extend-type TextFile
  IConvertToEDN
  (convert-to-edn [this]
    (TextFile->edn this))
  IRenderToHTML
  (render-to-html [this] this)
  ILoadFromResource
  (load-from-resource [{:keys [content]}]
    (-> content
        open-resource-if-exist
        ->Text))
  IConvertToMenu
  (convert-to-menu [this _] this))

