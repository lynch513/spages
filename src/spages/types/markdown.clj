(ns spages.types.markdown
  (:require [markdown.core          :refer [md-to-html-string]]
            [spages.io.toedn        :refer [IConvertToEDN]]
            [spages.io.tohtml       :refer [IRenderToHTML]]
            [spages.io.fromresource :refer [ILoadFromResource]]
            [spages.io.tomenu       :refer [IConvertToMenu]]
            [spages.utils           :refer [open-resource-if-exist]]
            [hiccup.util            :refer [escape-html]]))

;;-| Type for markdown
;;-------------------------------------------------------------------------------

(defrecord Markdown [content])

(defn Markdown? 
  "Check given argument i is Markdown"
  [i]
  (= spages.types.markdown.Markdown (type i)))

(defn Markdown->edn 
  "Creates a different edn format for the Markdown"
  [^Markdown i]
  (str "#Markdown " (prn-str (:content i))))

(defn edn->Markdown 
  "Takes the alternative edn and convert it into a Markdown"
  [c]
  (->Markdown c))

(extend-type Markdown
  IConvertToEDN
  (convert-to-edn [this]
    (Markdown->edn this))
  IRenderToHTML
  (render-to-html [{:keys [content]}]
    (-> content
        (escape-html)
        (md-to-html-string)))
  ILoadFromResource
  (load-from-resource [this] this)
  IConvertToMenu
  (convert-to-menu [this _] this))

;;-| Type for file with markdown
;;-------------------------------------------------------------------------------

(defrecord MarkdownFile [content])

(defn MarkdownFile? 
  "Check given argument i is MarkdownFile"
  [i]
  (= spages.types.markdown.MarkdownFile (type i)))

(defn MarkdownFile->edn 
  "Creates a different edn format for the MarkdownFile"
  [^MarkdownFile f]
  (str "#MarkdownFile " (prn-str (:content f))))

(defn edn->MarkdownFile 
  "Takes the alternative edn and convert it into a MarkdownFile"
  [c]
  (->MarkdownFile c))

(extend-type MarkdownFile
  IConvertToEDN
  (convert-to-edn [this]
    (MarkdownFile->edn this))
  IRenderToHTML
  (render-to-html [this] this)
  ILoadFromResource
  (load-from-resource [{:keys [content]}]
    (-> content
        open-resource-if-exist
        ->Markdown))
  IConvertToMenu
  (convert-to-menu [this _] this))
