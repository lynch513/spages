(ns spages.types.site
  (:require [spages.io.toedn        :refer [IConvertToEDN]]
            [spages.io.tohtml       :refer [IRenderToHTML render-to-html]]
            [spages.io.tomenu       :refer [IConvertToMenu convert-to-menu]]
            [spages.io.fromresource :refer [ILoadFromResource load-from-resource]]
            [spages.types.menu      :refer :all]))

;;-| Type for static site
;;-------------------------------------------------------------------------------

(defrecord Site [title context child])

(defn Site? 
  "Check given argument i is Site"
  [i]
  (= spages.types.site.Site (type i)))

(defn Site->edn 
  "Creates a different edn format for the Site"
  [^Site i]
  (str "#Site " (prn-str (into {} i))))

(defn edn->Site 
  "Takes the alternative edn and convert it into a Site"
  [m]
  (map->Site m))

(extend-type Site
  IConvertToEDN
  (convert-to-edn [this]
    (Site->edn this))
  IRenderToHTML
  (render-to-html [{:keys [title context child]}] 
    (->Site
      (render-to-html title)
      (render-to-html context)
      (render-to-html child)))
  ILoadFromResource
  (load-from-resource [{:keys [title context child]}] 
    (->Site
      (load-from-resource title)
      (load-from-resource context)
      (load-from-resource child)))
  IConvertToMenu
  (convert-to-menu [{:keys [title _ child]} path]
    (->MenuSite
      title
      (convert-to-menu child path))))

;;-| Type for static page
;;-------------------------------------------------------------------------------

(defrecord Page [slug title context])

(defn Page? 
  "Check given argument i is Page"
  [i]
  (= spages.types.site.Page (type i)))

(defn Page->edn 
  "Creates a different edn format for the Page"
  [^Page p]
  (str "#Page " (prn-str (into {} p))))

(defn edn->Page 
  "Takes the alternative edn and convert it into a Page"
  [m]
  (map->Page m))

(extend-type Page
  IConvertToEDN
  (convert-to-edn [this]
    (Page->edn this))
  IRenderToHTML
  (render-to-html [{:keys [slug title context]}] 
    (->Page
      slug
      (render-to-html title)
      (render-to-html context)))
  ILoadFromResource
  (load-from-resource [{:keys [slug title context]}] 
    (->Page
      slug
      title
      (load-from-resource context)))
  IConvertToMenu
  (convert-to-menu [{:keys [slug title]} path]
    (->MenuItem slug title path)))

;;-| Type for static directory
;;-------------------------------------------------------------------------------

(defrecord Dir [slug title context child])

(defn Dir? 
  "Check given argument i is Page"
  [i]
  (= spages.types.site.Dir (type i)))

(defn Dir->edn 
  "Creates a different edn format for the Dir"
  [^Dir d]
  (str "#Dir " (prn-str (into {} d))))

(defn edn->Dir 
  "Takes the alternative edn and convert it into a Dir"
  [m]
  (map->Dir m))

(extend-type Dir
  IConvertToEDN
  (convert-to-edn [this]
    (Dir->edn this))
  IRenderToHTML
  (render-to-html [{:keys [slug title context child]}] 
    (->Dir
      slug
      (render-to-html title)
      (render-to-html context)
      (render-to-html child)))
  ILoadFromResource
  (load-from-resource [{:keys [slug title context child]}] 
    (->Dir
      slug
      title
      (load-from-resource context)
      (load-from-resource child)))
  IConvertToMenu
  (convert-to-menu [{:keys [slug title context child]} path]
    (->MenuDir
      slug
      title
      path
      (convert-to-menu child (conj path slug)))))
