(ns spages.types.menu
  (:require [spages.io.toedn :refer [IConvertToEDN]]))

;;-| Type for site menu
;;-------------------------------------------------------------------------------

(defrecord MenuSite [title child])

(defn MenuSite? 
  "Check given argument i is MenuSite"
  [i]
  (= spages.types.menu.MenuSite (type i)))

(defn MenuSite->edn 
  "Creates a different edn format for the MenuSite"
  [^MenuSite i]
  (str "#MenuSite " (prn-str (into {} i))))

;; find me in spages.toedn/convert-to-edn 
(extend-protocol IConvertToEDN
  MenuSite
  (convert-to-edn [this]
    (MenuSite->edn this)))

(defn edn->MenuSite 
  "Takes the alternative edn and convert it into a MenuSite"
  [m]
  (map->MenuSite m))

;;-| Type for menu item
;;-------------------------------------------------------------------------------

(defrecord MenuItem [slug title path])

(defn MenuItem? 
  "Check given argument i is MenuItem"
  [i]
  (= spages.types.menu.MenuItem (type i)))

(defn MenuItem->edn 
  "Creates a different edn format for the MenuItem"
  [^MenuItem i]
  (str "#MenuItem " (prn-str (into {} i))))

;; find me in spages.toedn/convert-to-edn 
(extend-protocol IConvertToEDN
  MenuItem
  (convert-to-edn [this]
    (MenuItem->edn this)))

(defn edn->MenuItem 
  "Takes the alternative edn and convert it into a MenuItem"
  [m]
  (map->MenuItem m))

;;-| Type for menu dir
;;-------------------------------------------------------------------------------

(defrecord MenuDir [slug title path child])

(defn MenuDir? 
  "Check given argument i is MenuDir"
  [item]
  (= spages.types.menu.MenuDir (type item)))

(defn MenuDir->edn 
  "Creates a different edn format for the MenuDir"
  [^MenuDir d]
  (str "#MenuDir " (prn-str (into {} d))))

;; find me in spages.toedn/convert-to-edn 
(extend-protocol IConvertToEDN
  MenuDir
  (convert-to-edn [this]
    (MenuDir->edn this)))

(defn edn->MenuDir 
  "Takes the alternative edn and convert it into a MenuDir"
  [m]
  (map->MenuDir m))
