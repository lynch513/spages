(ns spages.io.tomenu)

(defprotocol IConvertToMenu
  (convert-to-menu [this path]))

(defn make-menu [i]
  (convert-to-menu i []))

;; Methods for simple data types
(extend-protocol IConvertToMenu
  nil
  (convert-to-menu [this _] this)
  java.lang.String
  (convert-to-menu [this _] this)
  java.lang.Long
  (convert-to-menu [this _] this)
  clojure.lang.PersistentVector
  (convert-to-menu [this path] (vec (map #(convert-to-menu % path) this)))
  clojure.lang.LazySeq
  (convert-to-menu [this path] (map #(convert-to-menu % path) this))
  clojure.lang.PersistentArrayMap
  (convert-to-menu [this path]
    (let [vs (vals this)
          ks (keys this)]
      (zipmap ks (map #(convert-to-menu % path) vs)))))
