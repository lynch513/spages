(ns spages.io.tohtml
  (:require [hiccup.util :refer [escape-html]]))

(defprotocol IRenderToHTML
  (render-to-html [this]))

;; Methods for simple data types
(extend-protocol IRenderToHTML
  nil
  (render-to-html [this] this)
  java.lang.String
  (render-to-html [this] (escape-html this))
  java.lang.Long
  (render-to-html [this] this)
  clojure.lang.PersistentVector
  (render-to-html [this] (vec (map render-to-html this)))
  clojure.lang.LazySeq
  (render-to-html [this] (map render-to-html this))
  clojure.lang.PersistentArrayMap
  (render-to-html [this]
    (let [vs (vals this)
          ks (keys this)]
      (zipmap ks (map render-to-html vs))))
  )
