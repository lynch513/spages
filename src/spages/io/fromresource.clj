(ns spages.io.fromresource)

(defprotocol ILoadFromResource
  (load-from-resource [this]))

;; Methods for simple data types
(extend-protocol ILoadFromResource
  nil
  (load-from-resource [this] this)
  java.lang.String
  (load-from-resource [this] this)
  java.lang.Long
  (load-from-resource [this] this)
  clojure.lang.PersistentVector
  (load-from-resource [this] (vec (map load-from-resource this)))
  clojure.lang.LazySeq
  (load-from-resource [this] (map load-from-resource this))
  clojure.lang.PersistentArrayMap
  (load-from-resource [this]
    (let [vs (vals this)
          ks (keys this)]
      (zipmap ks (map load-from-resource vs))))
  )
