(ns spages.io.toedn)

(defprotocol IConvertToEDN
  "Creates a different edn format for the given type"
  (convert-to-edn [this]))

