(ns spages.utils
  (:require [clojure.java.io :as io]))

;;-| Utils
;;-------------------------------------------------------------------------------

(defn open-resource-if-exist [file-name]
  (if-let [file-resource (io/resource file-name)]
    (slurp file-resource)
    (throw (java.io.FileNotFoundException. (format "file %s not found" file-name)))))

(comment (open-resource-if-exist "main.edn"))
