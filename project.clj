(defproject spages "0.1.0-alpha"
  :description "Library for create simple static model of pages for web sites and others"
  :url "https://bitbucket.org/lynch513/spages"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [hiccup              "1.0.5"]
                 [markdown-clj        "1.0.1"]]
  :main ^:skip-aot spages.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
