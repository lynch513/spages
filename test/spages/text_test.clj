(ns spages.text_test
  (:require [clojure.test           :refer :all]
            [clojure.edn            :as edn]
            [hiccup.util            :refer [escape-html]]
            [spages.types.text      :refer :all]
            [spages.io.fromresource :refer [load-from-resource]]
            [spages.io.tohtml       :refer [render-to-html]]
            [spages.io.tomenu       :refer [make-menu]]
            [spages.io.toedn        :refer [convert-to-edn]]))

(def t-Text      (->Text "this is test"))
(def t-html-Text (->Text "<h1>this is test</h1>"))
(def t-text-edn  "#Text \"this is test\"\n")
(def t-text-html "&lt;h1&gt;this is test&lt;/h1&gt;")

(testing "Testing type for Text ->"
  (testing "Check predicate for type"
    (is (Text? t-Text)))
  (testing "Creates a different edn format for the Text"
    (is (=
         t-text-edn
         (convert-to-edn t-Text))))
  (testing "Takes the alternative edn and convert it into a Text"
    (is (=
         t-Text
         (edn/read-string
           {:readers {'Text edn->Text}}
           t-text-edn))))
  (testing "Render text type to html (take content of Text and escape html)"
    (is (=
         t-text-html 
         (render-to-html t-html-Text))))
  (testing "Load from resource (skip for this type)"
    (is (=
         t-Text
         (load-from-resource t-Text))))
  (testing "Convert to menu (skip for this type)"
    (is (=
         t-Text
         (make-menu t-Text)))))


(def t-TextFile      (->TextFile "test/test.txt"))
(def t-textfile-edn  "#TextFile \"test/test.txt\"\n")
(def t-Text-1        (->Text "<h1>this is test\n"))

(testing "Testing type for TextFile ->"
  (testing "Check predicate for type"
    (is (TextFile? t-TextFile)))
  (testing "Creates a different edn format for the TextFile"
    (is (=
        t-textfile-edn
         (convert-to-edn t-TextFile))))
  (testing "Takes the alternative edn and convert it into a TextFile"
    (is (=
         t-TextFile 
         (edn/read-string 
           {:readers {'TextFile edn->TextFile}}
           t-textfile-edn))))
  (testing "Render text file to html (skip rendering)"
    (is (=
         t-TextFile
         (render-to-html t-TextFile))))
  (testing "Load text file (load from file, escape html and convert to simple text)"
    (is (=
         t-Text-1
         (load-from-resource t-TextFile)
         )))
  (testing "Convert to menu (skip for this type)"
    (is (=
         t-TextFile
         (make-menu t-TextFile)))))


