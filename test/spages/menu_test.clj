(ns spages.menu-test
  (:require [clojure.test      :refer :all]
            [clojure.edn       :as edn]
            [spages.types.menu :refer :all]
            [spages.io.toedn   :refer [convert-to-edn]]))

(def t-MenuSite (->MenuSite "test site" []))
(def t-menusite-edn "#MenuSite {:title \"test site\", :child []}\n")

(testing "MenuSite type testing"
  (testing "Check predicate for type"
    (is (MenuSite? t-MenuSite)))
  (testing "Creates a different edn format for the MenuSite"
    (is (=
         t-menusite-edn
         (convert-to-edn t-MenuSite))))
  (testing "Takes the alternative edn and convert it into a MenuSite"
    (is (=
         t-MenuSite
         (edn/read-string
           {:readers {'MenuSite edn->MenuSite}}
           t-menusite-edn)))))

(def t-MenuDir (->MenuDir :test "test" [:test] []))
(def t-menudir-edn "#MenuDir {:slug :test, :title \"test\", :path [:test], :child []}\n" )

(testing "Testing type for MenuDir ->"
  (testing "Check predicate for type"
    (is (MenuDir? t-MenuDir)))
  (testing "Creates a different edn format for the MenuDir"
    (is (=
         t-menudir-edn 
         (convert-to-edn t-MenuDir))))
  (testing "Takes the alternative edn and convert it into a MenuDir"
    (is (=
         t-MenuDir
         (edn/read-string 
           {:readers {'MenuDir edn->MenuDir}}
           t-menudir-edn)))))

(def t-MenuItem (->MenuItem :test "test" [:test]))
(def t-menuitem-edn "#MenuItem {:slug :test, :title \"test\", :path [:test]}\n" )

(testing "Testing type for MenuItem ->"
  (testing "Check predicate for type"
    (is (MenuItem? t-MenuItem)))
  (testing "Creates a different edn format for the MenuItem"
    (is (=
         t-menuitem-edn 
         (convert-to-edn t-MenuItem))))
  (testing "Takes the alternative edn and convert it into a MenuItem"
    (is (=
         t-MenuItem
         (edn/read-string 
           {:readers {'MenuItem edn->MenuItem}}
           t-menuitem-edn)))))

