(ns spages.core-test
  (:require [clojure.test           :refer :all]
            [spages.core            :refer :all]
            [spages.types.site      :refer :all]
            [spages.io.tohtml       :refer [render-to-html]]
            [spages.io.fromresource :refer [load-from-resource]]))

(def t-data (-> "main3.edn"
                (parse-edn-resource)
                (load-from-resource)
                (render-to-html)))
(def t-Page (->Page :index "Index page from root" {}))
(def t-nested-Page (->Page :test-page "Test page" {}))

(testing "Testing get-page function"
  (testing "Get page from site root"
    (is (=
         t-Page
         (get-page t-data :index))))
  (testing "Get nested page"
    (is (=
         t-nested-Page
         (get-page t-data :main :test-dir :test-page))))
  (testing "Return page by using strings in path"
    (is (=
         t-nested-Page
         (get-page t-data "main" "test-dir" "test-page"))))
  (testing "Return nil if page not found"
    (is (=
         nil
         (get-page t-data :main-2 :index))))
  (testing "Return nil if page not found and using strings in path"
    (is (=
         nil
         (get-page t-data "main-2" "index")))))

(def t-data-with-duplicate-slugs (-> "main5.edn"
                                     (parse-edn-resource)
                                     (load-from-resource)
                                     (render-to-html)))

(testing "Testing on model with duplicated slugs"
  (testing "Return first page with the same slugs with get-page"
    (is (=
         t-Page
         (get-page t-data-with-duplicate-slugs :index))))
  (testing "Return first page with the same slugs with get-in"
    (is (=
         (into {} t-Page)
         (get-in (make-routing-table t-data-with-duplicate-slugs) [:index])))))

(def t-Dir (->Dir :test-dir "Test dir" nil [t-nested-Page]))

(testing "Testing get-dir function"
  (testing "Get nested dir from site"
    (is (=
         t-Dir
         (get-dir t-data :main :test-dir))))
  (testing "Get dir from site by using strings in path"
    (is (=
         t-Dir
         (get-dir t-data "main" "test-dir"))))
  (testing "Return nil if dir not found"
    (is (=
         nil
         (get-dir t-data :test-dir-2))))
  (testing "Return nil if dir not found and using strings in path"
    (is (=
         nil
         (get-dir t-data "test-dir-2"))))
  )

(def t-routing-table (-> "main4.edn"
                         (parse-edn-resource)
                         (load-from-resource)
                         (render-to-html)
                         (make-routing-table)))
(def t-index-page {:slug :index :title "Index page from root" :context {}})
(def t-test-page {:slug :test-page :title "Test page" :context {}})
(def t-test-dir {:test-in-dir-1 {} :test-in-dir-2 {} :test-page t-test-page})

(testing "Testing make-route-table function"
  (testing "Get page from map root"
    (is (=
         t-index-page
         (get-in t-routing-table [:index]))))
  (testing "Get nested page from map"
    (is (=
         t-test-page
         (get-in t-routing-table [:main :test-dir :test-page]))))
  (testing "Get nested dir from map"
    (is (=
         t-test-dir
         (get-in t-routing-table [:main :test-dir])))))
