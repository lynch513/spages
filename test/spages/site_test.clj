(ns spages.site-test
  (:require [clojure.test           :refer :all]
            [clojure.edn            :as edn]
            [spages.types.site      :refer :all]
            [spages.types.menu      :refer :all]
            [spages.io.fromresource :refer [load-from-resource]]
            [spages.io.tohtml       :refer [render-to-html]]
            [spages.io.tomenu       :refer [make-menu convert-to-menu]]
            [spages.io.toedn        :refer [convert-to-edn]]))

(def t-Site     (->Site "test site" {:host "localhost"} []))
(def t-site-edn "#Site {:title \"test site\", :context {:host \"localhost\"}, :child []}\n" )
(def t-MenuSite (->MenuSite "test site" []))

(testing "Testing type for Site ->"
  (testing "Check predicate for type"
    (is (Site? t-Site)))
  (testing "Creates a different edn format for the Site"
    (is (=
         t-site-edn 
         (convert-to-edn t-Site))))
  (testing "Takes the alternative edn and convert it into a Site"
    (is (=
         t-Site
         (edn/read-string 
           {:readers {'Site edn->Site}}
           t-site-edn))))
  (testing "Render site to html (skip rendering)"
    (is (=
         t-Site
         (render-to-html t-Site))))
  (testing "Load from resource (skip for this type)"
    (is (=
         t-Site
         (load-from-resource t-Site))))
  (testing "Convert to menu"
    (is (=
         t-MenuSite
         (make-menu t-Site)))))

(def t-Page (->Page :test "test" {:description "test" :short "test" :content []}))
(def t-page-edn "#Page {:slug :test, :title \"test\", :context {:description \"test\", :short \"test\", :content []}}\n" )
(def t-MenuItem (->MenuItem :test "test" []))
(def t-MenuItem-path (assoc t-MenuItem :path [:test]))

(testing "Testing type for Page ->"
  (testing "Check predicate for type"
    (is (Page? t-Page)))
  (testing "Creates a different edn format for the Page"
    (is (=
         t-page-edn
         (convert-to-edn t-Page))))
  (testing "Takes the alternative edn and convert it into a Page"
    (is (=
         t-Page
         (edn/read-string 
           {:readers {'Page edn->Page}}
           t-page-edn))))
  (testing "Render page to html (skip rendering)"
    (is (=
         t-Page
         (render-to-html t-Page))))
  (testing "Load from resource (skip for this type)"
    (is (=
         t-Page
         (load-from-resource t-Page))))
  (testing "Convert to menu"
    (is (=
         t-MenuItem
         (make-menu t-Page))))
  (testing "Convert to menu and receive path"
    (is (=
         t-MenuItem-path
         (convert-to-menu t-Page [:test])))))

(def t-Dir (->Dir :test "test" {:meta "meta dir"} []))
(def t-dir-edn "#Dir {:slug :test, :title \"test\", :context {:meta \"meta dir\"}, :child []}\n" )
(def t-MenuDir-path (->MenuDir :test "test" [:test-path] []))
(def t-MenuDir-with-child (assoc t-Dir :child [t-Page]))

(testing "Testing type for Dir ->"
  (testing "Check predicate for type"
    (is (Dir? t-Dir)))
  (testing "Creates a different edn format for the Dir"
    (is (=
         t-dir-edn 
         (convert-to-edn t-Dir))))
  (testing "Takes the alternative edn and convert it into a Dir"
    (is (=
         t-Dir
         (edn/read-string 
           {:readers {'Dir edn->Dir}}
           t-dir-edn))))
  (testing "Render dir to html (skip rendering)"
    (is (=
         t-Dir
         (render-to-html t-Dir))))
  (testing "Load from resource (skip for this type)"
    (is (=
         t-Dir
         (load-from-resource t-Dir))))
  (testing "Convert to menu and receive path"
    (is (=
         t-MenuDir-path
         (convert-to-menu t-Dir [:test-path]))))
  (testing "Convert to menu and transmit path"
    (is (=
         [:test] 
         (-> t-MenuDir-with-child
             (convert-to-menu [])
             (:child)
             (first)
             (:path))))))

