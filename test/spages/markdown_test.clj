(ns spages.markdown-test
  (:require [clojure.test           :refer :all]
            [clojure.edn            :as edn]
            [spages.types.markdown  :refer :all]
            [spages.io.fromresource :refer [load-from-resource]]
            [spages.io.tohtml       :refer [render-to-html]]
            [spages.io.tomenu       :refer [make-menu]]
            [spages.io.toedn        :refer [convert-to-edn]]))

(def t-Markdown      (->Markdown "this is test"))
(def t-markdown-edn  "#Markdown \"this is test\"\n")
(def t-markdown-html "<h1>this is test</h1>")
(def t-html-Markdown (->Markdown "# this is test"))

(testing "Testing type for Markdown ->"
  (testing "Check predicate for type"
    (is (Markdown? t-Markdown)))
  (testing "Creates a different edn format for the Markdown"
    (is (=
         t-markdown-edn
         (convert-to-edn t-Markdown))))
  (testing "Takes the alternative edn and convert it into a Markdown"
    (is (=
         t-Markdown
         (edn/read-string 
           {:readers {'Markdown edn->Markdown}}
           t-markdown-edn))))
  (testing "Render markdown to html (take content of Markdown and convert to html)"
    (is (=
         t-markdown-html
         (render-to-html t-html-Markdown))))
  (testing "Load from resource (skip for this type)"
    (is (=
         t-Markdown
         (load-from-resource t-Markdown))))
  (testing "Convert to menu (skip for this type)"
    (is (=
         t-Markdown
         (make-menu t-Markdown)))))

(def t-MarkdownFile     (->MarkdownFile "test/test.md"))
(def t-markdownfile-edn "#MarkdownFile \"test/test.md\"\n")
(def t-Markdown-1       (->Markdown "# this is test\n"))

(testing "Testing type for MarkdownFile ->"
  (testing "Check predicate for type"
    (is (MarkdownFile? t-MarkdownFile)))
  (testing "Creates a different edn format for the MarkdownFile"
    (is (=
         t-markdownfile-edn 
         (convert-to-edn t-MarkdownFile))))
  (testing "Takes the alternative edn and convert it into a MarkdownFile"
    (is (=
         t-MarkdownFile
         (edn/read-string 
           {:readers {'MarkdownFile edn->MarkdownFile}}
           t-markdownfile-edn))))
  (testing "Render markdown file to html (skip rendering)"
    (is (=
         t-MarkdownFile
         (render-to-html t-MarkdownFile))))
  (testing "Load markdown file (load from file, convert to html)"
    (is (=
         t-Markdown-1
         (load-from-resource t-MarkdownFile))))
  (testing "Convert to menu (skip for this type)"
    (is (=
         t-MarkdownFile
         (make-menu t-MarkdownFile)))))

  

