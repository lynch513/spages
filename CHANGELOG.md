# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Added
- Add convenient dsl for the static model. See resources/main2.edn and resources/main4.edn

## 0.1.0-alpha - 2018-04-18
### Added

- Types for text, markdown, menu, pages and dirs.
- Protocols for render to html, load from resource, parse from edn, constract menu
- Namespace spages.utils and open-resource-if-exist function
- Parse-edn-resource and read-string-with-types
- Function get-page, get-dir, make-routing-table
- Prepare project for deploy 

